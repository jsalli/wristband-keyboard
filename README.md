## Create a virtual environment
On Windows `python -m venv wb_venv` and `wb_venv\Scripts\Activate`

On Ubuntu `virtualenv wb_venv` and `source wb_venv/bin/activate`

## Install dependencies
`pip install -r requirements.txt`

## Sessenta Quattro communication testing
Run `python emg_logger.py` to connect to the Sessanto Quattro and Visualize it's channels on a plot. Remember to connect to the Sessanta Quattro's hot spot.

### Commandline parameters
There are 4 commandline parameters you can give:
- `test`
  - Specify test to run: vis or file
  - Options: vis (visualize data on screen) or file (Write data to file)
  - Default: vis
  - Short: `t`
- `number_of_samples`
  - Specify how many samples/rows to save to file if test-parameter is `file`
  - Default: 100
  - Short: `s`
- `file_name`
  - Specify file to save the data
  - Default: `raw_binary.bin`
  - Short: `f`
- `mode`
  - Specify the Sessanta Quattro's mode
  - Options: See Sessanta Quattoro's communication protocol
  - Default: 0
  - Short: `m`

Example 1:
python emg_logger.py --t=vis --m=1

Example 2:
python emg_logger.py --t=file --s=1000 --f=my-file.whatever --m=2


## Keylogger testing
Run `python keyboard_logger.py`


## Dataset generation
Run `python data_set_creator.py` to connect to the Sessanto Quattro and generate dataset. Remember to connect to the Sessanta Quattro's hot spot. The dataset is a CSV-file. First columns will be channels. The last columns will be the onehot output of the keylogger. The number of channels and onehot output length is included in the generated output file's name.

### Commandline parameters
There are 3 commandline parameters you can give:
- `number_of_samples`
  - Specify how many samples/rows to save to file
  - Default: 100
  - Short: `s`
- `file_name`
  - Specify csv file name/path to save the data. Don't put extension. Externsion is always .csv
  - Default: recording
  - Short: `f`
- `mode`
  - Specify the Sessanta Quattro's mode
  - Options: See Sessanta Quattoro's communication protocol
  - Default: 0
  - Short: `m`

Example 1:
python data_set_creator.py --s=10000 --f=my-recording --m=3

Example 2:
python data_set_creator.py --number_of_samples=1000 --file_name=my-file --mode=1
