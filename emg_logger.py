import socket
import numpy as np


COMMAND_LENGTH_IN_BYTES = 2

class EMGLogger():
    def __init__(self, ip, port):
        self._ip = ip
        self._port = port
        self._sq_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sq_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


    def create_bin_command(self, go=1, rec=0, trig=0, ext=0, hpf=0, hres=1, mode=0, nch=0, fsamp=0, getset=0):
        command = 0
        command = command + go
        command = command + rec * 2
        command = command + trig * 4
        command = command + ext * 16
        command = command + hpf * 64
        command = command + hres * 128
        command = command + mode * 256
        command = command + nch * 2048
        command = command + fsamp * 8192
        command = command + getset * 32768

        number_of_channels = None
        sample_frequency = None
        bytes_in_sample = None

        if nch == 0:
            if mode == 1:
                number_of_channels = 8
            else:
                number_of_channels = 12
        elif nch == 1:
            if mode == 1:
                number_of_channels = 12
            else:
                number_of_channels = 20
        elif nch == 2:
            if mode == 1:
                number_of_channels = 20
            else:
                number_of_channels = 36
        elif nch == 3:
            if mode == 1:
                number_of_channels = 36
            else:
                number_of_channels = 68
        else:
            raise Exception("wrong value for nch. Got: {}".format(nch))

        if fsamp == 0:
            if mode == 3:
                sample_frequency = 2000
            else:
                sample_frequency = 500
        elif fsamp == 1:
            if mode == 3:
                sample_frequency = 4000
            else:
                sample_frequency = 1000
        elif fsamp == 2:
            if mode == 3:
                sample_frequency = 8000
            else:
                sample_frequency = 2000
        elif fsamp == 3:
            if mode == 3:
                sample_frequency = 16000
            else:
                sample_frequency = 4000
        else:
            raise Exception("wrong value for fsamp. Got: {}".format(fsamp))
        
        if hres == 1:
            bytes_in_sample = 3
        else:
            bytes_in_sample = 2

        if number_of_channels is None or sample_frequency is None or bytes_in_sample is None:
            raise Exception("Could not set number_of_channels and/or  and/or bytes_in_sample")
        print(command)
        self._all_sample_values = np.zeros((1, number_of_channels), dtype=np.int32)
        self._one_sample_values = np.zeros((number_of_channels,), dtype=np.int32)

        return self._bitstring_to_bytes(command), number_of_channels, sample_frequency, bytes_in_sample


    def connect_to_sq(self, start_command):
        print("Server binding")
        self._sq_socket.bind((self._ip, self._port))
        print("Server listening")
        self._sq_socket.listen(1)
        self._conn, addr = self._sq_socket.accept()
        print("Connection address: {}".format(addr))
        self._conn.send(start_command)

    
    def disconnect_from_sq(self):
        if self._conn is not None:
            stop_command, _, __, ___ = self.create_bin_command(go=0)
            self._conn.send(stop_command)
            self._conn.shutdown(2)
            self._conn.close()
        else:
            raise Exception("Can't disconnect from because the connection is not established")


    def read_channels(self, number_of_channels, bytes_in_sample, number_of_samples, flatten=False):
        raw_data_stream = self.read_raw_bytes(number_of_channels, bytes_in_sample, number_of_samples, True)

        # Read the data row by row
        for row_index in range(number_of_samples):
            # Get one row
            row_start = row_index * number_of_channels * bytes_in_sample
            row_end = (row_index + 1) * number_of_channels * bytes_in_sample
            row = raw_data_stream[row_start:row_end]

            
            # Separate channels. One channel has "bytes_in_sample" many bytes in it.
            for channel_index in range(number_of_channels):
                channel_start = channel_index * bytes_in_sample
                channel_end = (channel_index + 1) * bytes_in_sample
                channel = row[channel_start:channel_end]

                value = None
                if bytes_in_sample == 2:
                    value = channel[0] * 256 + channel[1]

                if bytes_in_sample == 3:
                    # Combine 3 bytes to a 24 bit value
                    value = channel[0] * 65536 + channel[1] * 256 + channel[2]
                    # Search for the negative values and make the two's complement
                    if value >= 8388608:
                        value -= 16777216
                    
                else:
                    raise Exception("Unknown bytes_in_sample value. Got: {}, but expecting 2 or 3".format(bytes_in_sample))
                
                self._one_sample_values[channel_index] = value

            self._all_sample_values[row_index] = self._one_sample_values

        if flatten is True:
            return self._all_sample_values.flatten()
        return self._all_sample_values


    def read_raw_bytes(self, number_of_channels, bytes_in_sample, number_of_samples, accquire_aux_and_acc):
        if accquire_aux_and_acc is True:
            buffer_size = number_of_channels * bytes_in_sample * number_of_samples
            raw_data_stream = self._conn.recv(buffer_size)
            return raw_data_stream
        else:
            buffer_size = (number_of_channels - 4) * bytes_in_sample * number_of_samples
            raw_data_stream = self._conn.recv(buffer_size)

            # Read the auxiliary and accessory channels but don't use the data
            discard_buffer_size = 4 * bytes_in_sample * number_of_samples
            self._conn.recv(discard_buffer_size)
            return raw_data_stream


    def _bitstring_to_bytes(self, command):
        return int(command).to_bytes(COMMAND_LENGTH_IN_BYTES, byteorder = "big")


# For testing
FLAGS = None
# For testing
def start_visualization_test(TCP_IP, TCP_PORT):
    print("Starting visualization")
    emg_logger_connection = EMGLogger(TCP_IP, TCP_PORT)
    GRAPH_LENGTH = 500
    try:
        start_command, number_of_channels, sample_frequency, bytes_in_sample = emg_logger_connection.create_bin_command(mode=FLAGS.mode, nch=1, fsamp=3)
        emg_logger_connection.connect_to_sq(start_command)
        visualizer = Visualizer(number_of_channels, GRAPH_LENGTH)
        visualizer.start()
        while True:
            channel_values = emg_logger_connection.read_channels(number_of_channels, bytes_in_sample, number_of_samples=1)
            # print(channel_values)
            visualizer.update_values(channel_values)
        
    except KeyboardInterrupt:
        emg_logger_connection.disconnect_from_sq()
        visualizer.close()
    except Exception as e:
        print("=== ERROR")
        print(e)
        print("=== ERROR")
        emg_logger_connection.disconnect_from_sq()
        visualizer.close()

# For testing
def start_write_to_file_test(TCP_IP, TCP_PORT):
    file_name = FLAGS.file_name
    number_of_samples = FLAGS.number_of_samples
    emg_logger_connection = EMGLogger(TCP_IP, TCP_PORT)
    try:
        start_command, number_of_channels, sample_frequency, bytes_in_sample = emg_logger_connection.create_bin_command(mode=FLAGS.mode)
        emg_logger_connection.connect_to_sq(start_command)
        print("Starting saving data to file '{}'".format(file_name))
        f = open(file_name, "wb")
        for _ in range(number_of_samples): # For 5 x 200 samples
            channel_values = emg_logger_connection.read_raw_bytes(number_of_channels, bytes_in_sample, number_of_samples=1, accquire_aux_and_acc=True)
            # print(channel_values)
            f.write(channel_values)
        f.close()

        emg_logger_connection.disconnect_from_sq()
        
    except KeyboardInterrupt:
        emg_logger_connection.disconnect_from_sq()
    pass


def main(_):
    TCP_IP = "0.0.0.0"
    TCP_PORT = 45454
    test = FLAGS.test
    if test == "file":
        start_write_to_file_test(TCP_IP, TCP_PORT)
    elif test == "vis": 
        start_visualization_test(TCP_IP, TCP_PORT)
    else:
        raise Exception("Unknown test to run. Got: '{}'".format(test))


# For testing
if __name__ == "__main__":
    from absl import app
    from absl import flags
    import time
    from visualizer import Visualizer

    flags.DEFINE_string("test", "vis", "Specify test to run: vis or file", short_name="t")
    flags.DEFINE_integer("mode", 0, "Specify the Sessanta Quattro\"s mode", short_name="m")
    flags.DEFINE_string("file_name", "raw_binary.bin", "Specify file to save the data", short_name="f")
    flags.DEFINE_integer("number_of_samples", 100, "Specify how many samples/rows to save to file", short_name="s")

    FLAGS = flags.FLAGS
    app.run(main)