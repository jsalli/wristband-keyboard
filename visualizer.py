import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from multiprocessing import Process, Array


class Visualizer():
    def __init__(self, channels, graph_length):
        self._channels = channels
        self._graph_length = graph_length
        self._shared_ys = Array('d', [0] * channels * graph_length)
        self._ys = np.zeros((channels * graph_length,))
        self._shared_ys[:] = self._ys.tolist()


    def start(self):
        self._p =Process(target=self._start, args=(
            self._shared_ys, self._graph_length, self._channels))
        self._p.start()

    
    def close(self):
        self._p.terminate()


    def _start(self, shared_ys, graph_length, channels):
        channel_min_max_memory = [[0, 0]] * channels
        xs = np.arange(0, graph_length, 1)
        fig, axes = plt.subplots(nrows=channels)
        styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-', 'r-', 'g-', 'y-', 'm-', 'k-', 'c-']

        def auto_update_scale(ax, channel_min, channel_max):
            ax.set_ylim(channel_min, channel_max)

        def plot(ax, style, i):
            channel_ys = shared_ys[i::channels]
            ax.set_ylim(auto=True)
            return ax.plot(xs, channel_ys, style, animated=True)[0]
        
        lines = [plot(ax, style, i) for ax, style, i in zip(axes, styles, range(channels))]

        def animate(_):
            for j, line in enumerate(lines):
                data = shared_ys[j::channels]
                line.set_ydata(data)
                channel_min_value = min(data)
                channel_max_value = max(data)
                
                update = False
                if channel_min_value is not channel_min_max_memory[j][0]:
                    channel_min_max_memory[j][0] = channel_min_value
                    update = True
                    
                if channel_max_value is not channel_min_max_memory[j][1]:
                    channel_min_max_memory[j][1] = channel_max_value
                    update = True
                
                if update is True:
                    auto_update_scale(axes[j], channel_min_value, channel_max_value)

            return lines

        ani = animation.FuncAnimation(fig, animate, interval=0, blit=True)
        plt.show()


    def update_values(self, channel_values):
        self._ys = np.roll(self._ys, -self._channels)
        self._ys[-self._channels:] = channel_values
        self._shared_ys[:] = self._ys.tolist()


# For testing
if __name__ == '__main__':
    import time
    import random
    CHANNELS = 4
    GRAPH_LENGTH = 100
    visualizer = Visualizer(CHANNELS, GRAPH_LENGTH)
    visualizer.start()

    try:
        while True:
            channel_values = [random.uniform(2, 3), random.uniform(-3, -2), random.uniform(-0.5, 0.5), random.uniform(-3, 3)]
            visualizer.update_values(channel_values)
            time.sleep(0.1)
    except KeyboardInterrupt:
        visualizer.close()
    except Exception as e:
        print("=== ERROR")
        print(e)
        print("=== ERROR")
        visualizer.close()