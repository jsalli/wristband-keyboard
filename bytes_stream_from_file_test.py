import numpy as np
from visualizer import Visualizer
import time


def read_channels(raw_data_stream, number_of_channels, bytes_in_sample, number_of_samples):
    all_sample_values = np.zeros((number_of_samples, number_of_channels), dtype=np.int32)
    # Read the data row by row
    for row_index in range(number_of_samples):
        # Get one row
        row_start = row_index * number_of_channels * bytes_in_sample
        row_end = (row_index + 1) * number_of_channels * bytes_in_sample
        row = raw_data_stream[row_start:row_end]

        one_sample_values = np.zeros((number_of_channels,), dtype=np.int32)
        # Separate channels. One channel has "bytes_in_sample" many bytes in it.
        for channel_index in range(number_of_channels):
            channel_start = channel_index * bytes_in_sample
            channel_end = (channel_index + 1) * bytes_in_sample
            channel = row[channel_start:channel_end]

            value = None
            if bytes_in_sample == 2:
                value = channel[0] * 256 + channel[1]

            if bytes_in_sample == 3:
                # Combine 3 bytes to a 24 bit value
                value = channel[0] * 65536 + channel[1] * 256 + channel[2]
                # Search for the negative values and make the two's complement
                if value >= 8388608:
                    value -= 16777216
                
            else:
                raise Exception("Unknown bytes_in_sample value. Got: {}, but expecting 2 or 3".format(bytes_in_sample))
            
            one_sample_values[channel_index] = value

        all_sample_values[row_index] = one_sample_values

    return all_sample_values


def start():
    CHANNELS = 12
    GRAPH_LENGTH = 100
    visualizer = Visualizer(CHANNELS, GRAPH_LENGTH)
    visualizer.start()

    try:
        f = open("bin/raw_data_bytes.bin", "rb")
        stream = f.read()
        # for _ in range(20):
        channels = read_channels(stream, 12, 3, 50)

        for row in channels:
            print("=====row")
            print(row)
            visualizer.update_values(row)
            time.sleep(0.5)
    except KeyboardInterrupt:
        visualizer.close()
        f.close()

if __name__ == "__main__":
    start()
