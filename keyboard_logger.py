from pynput import keyboard
from pynput.keyboard import Key
from multiprocessing import Process, Value, Array


class KeyboardLogger():
    def __init__(self):
        self._active_key = Value("i", 1) #i represents signed integer
        self._active_label_vector = Array("i", [0,0,0,0,0,0,0,0,0,0])
        self._finger_key_mapping = ["q", "w", "e", "r", "v", "n", "u", "i", "o", "p"]


    def get_finger_key_mapping_length(self):
        return len(self._finger_key_mapping)

    def get_active_key_ascii(self):
        return self._active_key.value


    def get_active_label_vector(self):
        return self._active_label_vector[:]


    def start(self):
        self._p = Process(target=self._start, args=(self._active_key,))
        self._p.start()


    def close(self):
        self._p.terminate()


    def _set_active_label_vector(self, key, value):
        if hasattr(key, "char") is False:
            return
        
        found = None
        try:
            found = self._finger_key_mapping.index(key.char)
        except ValueError:
            found = None
        
        if found is not None:
            self._active_label_vector[found] = value


    def _on_press(self, key):
        self._active_key.value = ord(getattr(key, "char", "0"))
        # print(key)
        self._set_active_label_vector(key, 1)


    def _on_release(self, key):
        self._active_key.value = -1
        # print(key)
        self._set_active_label_vector(key, 0)
        
        if key == Key.esc:
            #Stop listener
            return False


    def _start(self, shared_value):
        print("Starting process")
        try:
            with keyboard.Listener(on_press=self._on_press, on_release=self._on_release) as listener:
                listener.join()
        except Exception as e:
            print("====ERROR =====")
            print(e)
            print("====ERROR =====")


# For testing
if __name__ == "__main__":
    import time
    keyboard_list = KeyboardLogger()
    keyboard_list.start()

    while True:
        key_value = keyboard_list.get_active_key_ascii()
        label_vector = keyboard_list.get_active_label_vector()
        print(key_value)
        print(label_vector)
        time.sleep(0.05)