from absl import app
from absl import flags

import numpy as np
import csv

from emg_logger import EMGLogger
from keyboard_logger import KeyboardLogger

flags.DEFINE_string("file_name", "recording", "Specify csv file name/path to save the data. Don't put extension. Externsion is always .csv", short_name="f")
flags.DEFINE_integer("number_of_samples", 100, "Specify how many samples/rows to save to file", short_name="s")
flags.DEFINE_integer("mode", 0, "Specify Sessanta Quattro's mode", short_name="m")

FLAGS = flags.FLAGS

TCP_IP = "0.0.0.0"
TCP_PORT = 45454


def bytes_to_integer(s):
    integer = int.from_bytes(s, byteorder="big")
    return integer


def start(_):
    number_of_samples = FLAGS.number_of_samples
    file_name = FLAGS.file_name
    mode = FLAGS.mode

    emg_logger_connection = EMGLogger(TCP_IP, TCP_PORT)
    keyboard_logger_connection = KeyboardLogger()
    try:
        start_command, number_of_channels, sample_frequency, bytes_in_sample = emg_logger_connection.create_bin_command(mode=mode, nch=1)
        emg_logger_connection.connect_to_sq(start_command)
        keyboard_logger_connection.start()
        active_keys_length = keyboard_logger_connection.get_finger_key_mapping_length()
        output_file_name = "{}_channels={},activekeyslength={}.csv".format(file_name, number_of_channels, active_keys_length)
        with open(output_file_name, "w", newline="") as csvfile:
            filewriter = csv.writer(csvfile, delimiter=",",
                            quotechar="|", quoting=csv.QUOTE_MINIMAL)

            sample_count = 0
            while sample_count < number_of_samples:
                channel_values = emg_logger_connection.read_channels(number_of_channels, bytes_in_sample, number_of_samples=1, flatten=True)
                active_label_vector = keyboard_logger_connection.get_active_label_vector()
                # print(channel_values)
                # print(active_label_vector)
                row_data = np.append(channel_values, active_label_vector)
                # print(row_data)
                filewriter.writerow(row_data.tolist())
                sample_count += 1
        
    except KeyboardInterrupt:
        print("Exiting...")
    finally:
        print("Closing connections...")
        emg_logger_connection.disconnect_from_sq()
        keyboard_logger_connection.close()


if __name__ == "__main__":
    app.run(start)