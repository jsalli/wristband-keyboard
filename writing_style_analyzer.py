from pynput import keyboard
from pynput.keyboard import Key
import time
import collections


key_down_dict = {}
time_between_keys_start = -1

first_key = True
key_press_count = 0
multiple_simultanious_key_press_count = 0

DISCARD_MULTIPLIER = 10
AVERAGE_COUNT_LENGTH = 25
average_key_press_duration = collections.deque([1], AVERAGE_COUNT_LENGTH)
average_time_between_key_presses = collections.deque([1], AVERAGE_COUNT_LENGTH)


def get_key(pressed_key):
    # Get non special key
    key = getattr(pressed_key, "char", None)
    # Get special key if a non special key was not pressed
    if key is None:
        if pressed_key == Key.space:
            key = "space"
        elif pressed_key == Key.enter:
            key = "enter"
    else:
        key = key.lower()
    
    return key


def set_time_between_key_presses():
    global time_between_keys_start, average_time_between_key_presses
    
    # On the very first key pressed there is no time_between_keys_start because it has not been set yet
    if time_between_keys_start < 0:
        return
    
    stop_time = time.time()
    time_between_key_presses = stop_time - time_between_keys_start

    average_val = sum(average_time_between_key_presses) / len(average_time_between_key_presses)
    # If the time between key presses is very long don't take it into account.
    # It might not be relevant. The person is thinking the next work or other things
    if time_between_key_presses < DISCARD_MULTIPLIER * average_val:
        average_time_between_key_presses.append(time_between_key_presses)
    else:
        print(" |Long wait, measurements of this press discarded| ")
    time_between_keys_start = -1


def on_press(pressed_key):
    global key_press_count, key_down_dict

    key = get_key(pressed_key)
    if key is not None:
        key_press_count += 1
        if key not in key_down_dict:
            key_down_dict[key] = time.time()
    
    set_time_between_key_presses()


def on_release(pressed_key):
    global average_key_press_duration, key_press_count
    global time_between_keys_start, multiple_simultanious_key_press_count

    if pressed_key == Key.esc:
        #Stop listener
        return False
    
    global first_key
    if first_key == True and pressed_key == Key.enter:
        first_key = False
        return
    
    key = get_key(pressed_key)
    if key is None:
        return
    
    if key not in key_down_dict:
        raise ValueError("Key: '{}' not pressed down before releasing".format(key))
    start_time = key_down_dict[key]
    end_time = time.time()
    duration = end_time - start_time

    average_key_press_duration.append(duration)
    
    del key_down_dict[key]

    # If time_between_keys_start has already been set it means that on_press-method has not yet been called to clear it
    # This means that two keys are hold down at the same time
    if time_between_keys_start > 0:
        multiple_simultanious_key_press_count += 1
    time_between_keys_start = time.time()


def start():
    print("Starting listener")
    start_time = time.time()
    try:
        with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
            listener.join()
    except KeyboardInterrupt:
        print("====KeyboardInterrupt =====")
    except Exception as e:
        print("====ERROR =====")
        print(e)
        print("====ERROR =====")
    finally:
        stop_time = time.time()
        print("")
        print("===")

        print("= Typing time: {:{width}.{prec}f}s".format((stop_time - start_time), width=4, prec=2))
        print("= Key press count: {}".format(key_press_count))

        print("===")

        average_val = sum(average_key_press_duration) / len(average_key_press_duration)
        max_val = max(average_key_press_duration)
        min_val = min(average_key_press_duration)
        print("= average_key_press_duration: {:{width}.{prec}f}s".format(average_val, width=4, prec=2))
        print("= max_key_press_duration: {:{width}.{prec}f}s".format(max_val, width=4, prec=2))
        print("= min_key_press_duration: {:{width}.{prec}f}s".format(min_val, width=4, prec=2))
        
        print("===")

        average_val = sum(average_time_between_key_presses) / len(average_time_between_key_presses)
        max_val = max(average_time_between_key_presses)
        min_val = min(average_time_between_key_presses)
        print("= average_time_between_key_presses: {:{width}.{prec}f}s".format(average_val, width=4, prec=2))
        print("= max_time_between_key_presses: {:{width}.{prec}f}s".format(max_val, width=4, prec=2))
        print("= min_time_between_key_presses: {:{width}.{prec}f}s".format(min_val, width=4, prec=2))
        print("= multiple_simultanious_key_press_count: {}".format(multiple_simultanious_key_press_count))


# For testing
if __name__ == "__main__":
    start()